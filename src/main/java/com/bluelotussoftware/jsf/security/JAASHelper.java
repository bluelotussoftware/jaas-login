/*
 *
 * Blue Lotus Software, LLC.
 *
 * Copyright 2012. All Rights Reserved.
 *
 * $Id$
 *
 */
package com.bluelotussoftware.jsf.security;

import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.resource.spi.security.PasswordCredential;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

/**
 *
 * @author John Yeary <john.yeary@bluelotussoftware.com>
 * @version 1.0.0
 */
public class JAASHelper {

    private LoginContext loginContext;
    private String realmName;
    private static final Logger LOGGER = Logger.getLogger(JAASHelper.class.getName());

    public JAASHelper(final String realmName) {
        this.realmName = realmName;
    }

    public JAASHelper() {
        this("fileLogin");
    }

    public boolean authenticate(String username, String password) {
        boolean result = false;
        LOGGER.log(Level.INFO, "Attempting to authenticate user: {0} with password: {1}", new Object[]{username, password});
        try {

            Subject subject = new Subject();
            subject.getPrincipals().add(new PrincipalImpl(username));
            PasswordCredential pc = new PasswordCredential(username, password.toCharArray());
            subject.getPrivateCredentials().add(pc);

            loginContext = new LoginContext(realmName, subject, new LoginCallback(username, password));
            loginContext.login();
            result = true;
        } catch (LoginException e) {
            LOGGER.log(Level.SEVERE, "Failed to login", e);
        }
        return result;
    }

    public Subject getSubject() {
        Subject result = null;
        if (null != loginContext) {
            result = loginContext.getSubject();
        }
        return result;
    }

    public static class LoginCallback implements CallbackHandler {

        private String userName = null;
        private String password = null;

        public LoginCallback(String userName, String password) {
            this.userName = userName;
            this.password = password;
        }

        @Override
        public void handle(Callback[] callbacks) {
            for (int i = 0; i < callbacks.length; i++) {
                if (callbacks[i] instanceof NameCallback) {
                    NameCallback nc = (NameCallback) callbacks[i];
                    nc.setName(userName);
                } else if (callbacks[i] instanceof PasswordCallback) {
                    PasswordCallback pc = (PasswordCallback) callbacks[i];
                    pc.setPassword(password.toCharArray());
                }
            }
        }
    }

    class PrincipalImpl implements Principal {

        private final String name;

        public PrincipalImpl(String name) {
            this.name = name;
        }


        @Override
        public String getName() {
        return this.name;
        }

    }
}
