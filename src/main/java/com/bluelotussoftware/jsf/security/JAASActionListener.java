/*
 *
 * Blue Lotus Software, LLC.
 *
 * Copyright 2012. All Rights Reserved.
 *
 * $Id$
 *
 */
package com.bluelotussoftware.jsf.security;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;

/**
 *
 * @author John Yeary <john.yeary@bluelotussoftware.com>
 * @version 1.0
 */
public class JAASActionListener implements ActionListener {

    private final ActionListener parent;
    private static final Logger LOGGER = Logger.getLogger(JAASActionListener.class.getName());

    public JAASActionListener(ActionListener parent) {
        this.parent = parent;
    }

    @Override
    public void processAction(ActionEvent event) throws AbortProcessingException {
        LOGGER.info("Processing JAAS ActionEvent");
        FacesContext context = FacesContext.getCurrentInstance();
        UIOutput component;
        String username = null, password = null;
        JAASHelper jaasHelper = new JAASHelper();

        // Check to see if they are on the login page.
        boolean onLoginPage = (-1 != context.getViewRoot().getViewId().lastIndexOf("login")) ? true : false;
        
        if (onLoginPage) {
            if (null != (component = (UIOutput) context.getViewRoot().findComponent("loginForm:username"))) {
                username = (String) component.getValue();
                LOGGER.log(Level.INFO, "username ==> {0}", username);
            }

            if (null != (component = (UIOutput) context.getViewRoot().findComponent("loginForm:password"))) {
                password = (String) component.getValue();
                LOGGER.log(Level.INFO, "password ==> {0}", password);
            }

            if (username == null && password == null) {
                context.getApplication().getNavigationHandler().handleNavigation(context, null, "login");
                LOGGER.log(Level.INFO, "Username or password were null");
                return;
            }

            // If JAAS authentication failed
            if (!jaasHelper.authenticate(username, password)) {
                context.getApplication().getNavigationHandler().handleNavigation(context, null, "login");
                LOGGER.log(Level.INFO, "Failed to authenticate, redirecting back to login");
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Failed to authenticate, redirecting back to login",
                        "Failed to authenticate" + username + ", redirecting back to login page.");
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                return;
            } else {
                // Subject must not be null, since authentication succeeded
                assert (null != jaasHelper.getSubject());
                // Put the authenticated subject in the session.
                context.getExternalContext().getSessionMap().put("JAASSubject", jaasHelper.getSubject());
            }
        }
        parent.processAction(event);
    }
}
